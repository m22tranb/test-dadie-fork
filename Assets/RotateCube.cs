using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class RotateCube : MonoBehaviour
{
    public InputAction activateRotation = new InputAction();
    public float speed = 3.0f;
    bool change = false;
    

    // Start is called before the first frame update

    void Start()
    {
        activateRotation.Enable();

        activateRotation.performed += ctx => change = !change;

    }

    // Update is called once per frame
    void Update()
    {
         


        if (change){ 
            transform.Rotate(Vector3.up * Time.deltaTime * speed);
            Debug.Log("code fonctionnel");
        
        }

        
    }
}
